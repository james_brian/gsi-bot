## Setting up the Bot

Make sure the following are installed:
	
node.js  
npm  
python 3  
ngrok

Notes: If you want to test the robot locally, you must run it in a VM so it can properly get your messages.
In other words, if you do all of these steps locally, in your current OS / instance, the robot won't respond to your messages in Teams.

### Steps
	1. Clone this repository.
	2. Install Hubot: npm install -g yo generator-hubot
		a. From: https://hubot.github.com/docs/
	3. Install the MSTeams Bot Framework adapter for Hubot: npm install --save hubot-botframework
	  a. From: https://github.com/Microsoft/BotFramework-Hubot
	4. Run: npm install botbuilder@3.15.0
	  a. Workaround for: https://github.com/Microsoft/BotFramework-Hubot/issues/42
	5. Setup or get get the App ID and App Password from: https://dev.botframework.com/bots/new
	6. Set the App ID and App Password as Global Variables in the operating system.
	  a. The keys are listed under the Global Variables Section: https://github.com/Microsoft/BotFramework-Hubot
	    i. BOTBUILDER_APP_ID
      ii. BOTBUILDER_APP_PASSWORD
    b. https://en.wikipedia.org/wiki/Environment_variable , on windows setting it as a System Variable works fine
	7. Open an admin terminal(needed on windows to get the variables) and run the following command in the gsi-bot/gsibot/ directory 
		a. WINDOWS: .\bin\hubot -a botframework
		b. LINUX: ./bin/hubot -a botframework		
	8. From a separate terminal run: ngrok http -host-header=rewrite 8080
		a. Hubot defaults to localhost 8080 when it is run.
		b. This is needed to expose an end-point to Microsoft for testing.
	8. Go back to: https://dev.botframework.com/bots and click on your robot.
	9. Go to Settings. Set the Messaging endpoint to the ngrok https forwarding address plus "/api/messages" and save
		a. Example: https://ac045cb0.ngrok.io/api/messages
	10. You can now test the robot on the Microsoft page and it should respond.
	11. To personally test it on Teams follow this guide: https://docs.microsoft.com/en-us/microsoftteams/platform/concepts/bots/bots-test
