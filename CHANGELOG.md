# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [1.0.0] - 2019-02-13
### Added
- Created changelog file at gsi-bot/CHANGELOG.md
- Created setup file at gsi-bot/SETUP.md
- Added random response to "hello".
- Added /changeteam command.
- Added /help command with help text for each command.
- Added /scrum command and supporting functions.
- Added .json reading and saving functions for the question and answer files.
- Robot will now ask questions based on your team.
- Added /report command and supporting functions.
- Added .json reading functionality and neat printing for report message.
- Robot will now send you a report based on your team.
- Added timestamps for each response in the report.

### Changed
- Rewrote codebase to remove local variables allowing *graceful* concurrent user interaction.
- Updated the README.md and spun documentation off to separate changelog and setup files.

### Removed
- Robot no longer sends emails after a scrum is completed. This will be revisited at a later time
