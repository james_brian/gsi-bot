# GSIBot

A helpful robot for your team. Generates a daily report based on the self reported activities of your team members.

##### [Changelog](CHANGELOG.md)
##### [Setup](SETUP.md)

### Features
* Users set their team via chat.
* Bot's questions are configurable via .json file.
* Bot can handle concurrent conversations and reporting.
* Bot stores user responses separated by team.
* Bot returns a report of your team's responses via chat.

### Roadmap & To Do
* Create an admin role
* Admins create teams
* Admins change scrum questions
* Admins an get a list of their team members
* Schedule a message to be sent to team members at a specific time
* Schedule a re-occurring message to be sent to team members at a specific time
* Admins can edit the timed messages and when it sends
* List available teams
* Prevent team change if team doesn't exist

##### File Structure
```
gsi-bot
|--CHANGELOG.md
|--README.md
|--SETUP.md
|--gsibot
   |--answers
      |--codersanonymous  #This folder must be created for each team
         |--James Wong.json
   |--bin
   |--questions # Place question .json files here
      |--codersanonymous.json
   |--scripts
      |--extra.coffee
      |--gsibot.coffee
      |--help.coffee
   |--users #User .json files get saved here
|--pyscripts
```

gsihealthbot@gmail.com  
CArobot123