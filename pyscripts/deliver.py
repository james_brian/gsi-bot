import logging, smtplib, ssl, sys

from email import encoders
from email.mime.base import MIMEBase
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText

logging.basicConfig(filename='example.log',level=logging.DEBUG)

# This expects the program to be run with the filename
# Example "James Wong.csv"
try:
    answerFile = sys.argv[1]
except IndexError:
    print("Please run with filename as argument.")
    raise

username = answerFile[:-4]

port = 465
smtp_server = "smtp.gmail.com"
sender_email = "gsihealthbot@gmail.com"
receiver_email = "james.wong@gsihealth.com"
password = "CArobot123"
subject = "Daily Scrum Report"
body = """This messages is from GSIBot. {user} has submitted their scrum.""".format(user=username)

# Create a multipart message and set headers
message = MIMEMultipart()
message["From"] = sender_email
message["To"] = receiver_email
message["Subject"] = subject

message.attach(MIMEText(body, "plain"))

filename = "C:\\Users\\jwong\\Desktop\\gsi-bot\\gsibot\\answers\\" + answerFile

with open(filename, "rb") as attachment:
    # Add file as application/octet-stream
    # Email client can usually download this automatically as attachment
    part = MIMEBase("application", "octet-stream")
    part.set_payload(attachment.read())

# Encode file in ASCII characters to send by email
encoders.encode_base64(part)

# Add header as key/value pair to attachment part
part.add_header(
    "Content-Disposition",
    f"attachment; filename= {filename}",
)

message.attach(part)
finalMessage = message.as_string()

context = ssl.create_default_context()
with smtplib.SMTP_SSL(smtp_server, port, context=context) as server:
    server.login(sender_email, password)
    server.sendmail(sender_email, receiver_email, finalMessage)

logging.debug("delivered")
