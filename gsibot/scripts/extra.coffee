# Description:
#   This script holds extra and utility functions for gsibot.
#
# Dependencies:
#
#
# Configuration:
#
#
# Commands:
#   hubot <trigger> - <what the respond trigger does>
#   <trigger> - <what the hear trigger does>
#
# Notes:
#   <optional notes required for the script>
#
# Author:
#   <github username of the original script author>

fs = require 'fs'

module.exports = (robot) ->
  greetingText = ['Hello', 'Greetings', 'Hi', 'Hello there', 'Howdy']

  robot.hear /hello/i, (res) ->
    res.send res.random greetingText

  robot.hear /order66/i, (res) ->
    res.send "Executing..."
    #exec 'python C:\\Users\\jwong\\Desktop\\gsi-bot\\pyscripts\\deliver.py ' + '\"James Wong.csv\"'

  robot.hear /\/changeteam (.*)/i, (res) ->
    configureName = res.envelope.user.name
    jsonObject = {'username': configureName, 'team': res.match[1]}
    fs.writeFile "./users/#{configureName}.json", JSON.stringify(jsonObject), (error) ->
      if error
        console.error("Error writing user file: \n", error)
        res.send "Unable to change your team."
        return
    res.send "Team changed to #{res.match[1]}"
