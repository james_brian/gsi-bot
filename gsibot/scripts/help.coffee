# Description:
#   Holds /help responses for gsibot.
#
# Dependencies:
#
#
# Configuration:
#
#
# Commands:
#   /help - Sends users a message of available commands.
#   /help arg1 - Sends users specific information on a given command.
#
# Notes:
#   Responses are hardcoded for now.
#
# Author:
#

module.exports = (robot) ->

  help = "I respond to the following messages or commands. Add a command as a parameter to get specific help. eg. /help scrum \n\n
  hello \n\n
  /help \n\n
  /changeteam \n\n
  /scrum \n\n
  /report \n\n"

  helpAnswers = {
    "help" : "\/help - takes no other parameters. Cheeky...",
    "changeteam" : "\/changeteam - takes 1 parameter of team name. eg: \/changeteam codersanonymous \n\n Use to set or update your team. ",
    "scrum" : "\/scrum - takes no parameters. Use to initiate a personal scrum report. Sends you a set of questions based on your configured team.",
    "report" : "\/report - takes no parameters. Use to get a report of your team's scrum responses."}

  robot.hear /\/help (.*)|\/help/i, (res) ->
    if !res.match[1]
      res.send help
      return
    switch res.match[1]
      when "help" then res.send helpAnswers.help
      when "changeteam" then res.send helpAnswers.changeteam
      when "scrum" then res.send helpAnswers.scrum
      when "report" then res.send helpAnswers.report
      else res.send "Unable to find help for this function."
