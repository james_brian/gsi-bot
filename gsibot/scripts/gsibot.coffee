# Description:
#   Primary script for the gsibot.
#
# Dependencies:
#   "<module name>": "<module version>"
#
# Configuration:
#   BOTBUILDER_APP_ID, BOTBUILDER_APP_PASSWORD
#
# Commands:
#   /scrum - Initiates a scrum to ask configured questions and capture data.
#   /report - Generates a report based on team.
#
# Notes:
#   Apologies to anyone who reads this code and my future self.
#
# Author:
#   <github username of the original script author>

fs = require 'fs'
path = require 'path'
{exec} = require 'child_process'

module.exports = (robot) ->

  robot.hear /y/i, (res) ->
    if getRobotBrain(res, 'answerCheck')
      username = res.envelope.user.name
      userTeam = getUserTeam(res)
      userAnswers = {"answers" : getRobotBrain(res, 'userAnswerArray')}
      fs.writeFile "./answers/#{userTeam}/#{username}.json", JSON.stringify(userAnswers), (error) ->
        if error
          console.error("Error writing answers file: ", error)
          res.send "Error saving scrum."
          return
      robot.brain.set(username + 'userFileLocation', "./answers/#{username}.json")
      #exec 'python C:\\Users\\jwong\\Desktop\\gsi-bot\\pyscripts\\deliver.py ' + '\"' + username + '.csv\"'
      res.send "Sent"
      refreshBotData(res)

  robot.hear /n/i, (res) ->
    if getRobotBrain(res, 'answerCheck')
      res.send "Deleted"
      refreshBotData(res)

  robot.hear /.*/i, (res) ->
    if getRobotBrain(res, 'answering') # Expecting answer from user
      addAnswer(res)
      if getRobotBrain(res, 'userQuestionNumber') < getRobotBrain(res, 'userQuestionsLength')
        askQuestion(res)
      else
        setRobotBrain(res, 'answering', false)
        checkAnswerArray(res)

  robot.hear /\/scrum/i, (res) ->
    failCheck = buildQuestionArray(res)
    if !failCheck
      return

    setRobotBrain(res, 'answering', true)
    setRobotBrain(res, 'userQuestionNumber', 0)
    userAnswerArray = []
    setRobotBrain(res, 'userAnswerArray', userAnswerArray)
    setRobotBrain(res, 'prettyReportMessage', "")

    res.send "Initiating Scrum Data Collection."
    setTimeout () ->
      askQuestion(res)
    , 2000

  robot.hear /\/report/i, (res) ->
    reportMessage = ""
    userTeam = getUserTeam(res)
    try
      reportData = fs.readdirSync "./answers/#{userTeam}/"
    catch error
      console.error("Error reading team file: \n", error)
      res.send "Unable to read your team's directory."
      return
    for file in reportData
      try
        fileData = fs.readFileSync "./answers/#{userTeam}/#{file}"
      catch error
        console.error("Error reading user answer file: \n", error)
        return
      stats = fs.statSync("./answers/#{userTeam}/#{file}")
      dateSubmitted = new Date(stats.mtime)
      reportMessage = reportMessage.concat(file + "-" + dateSubmitted.toLocaleString("en-US", {timeZone: "America/New_York"}) + ": \n\n")
      userAnswers = JSON.parse(fileData)
      for answer in userAnswers.answers
        reportMessage = reportMessage.concat(answer + " \n\n")
      reportMessage = reportMessage.concat(">\n\n")
    res.send reportMessage

  checkAnswerArray = (res) ->
    prettyMessage = getRobotBrain(res, 'prettyReportMessage')
    res.send "#{prettyMessage}", "Would you like to send this? y/n"
    setRobotBrain(res, 'answerCheck', true)

  buildQuestionArray = (res) -> # returns false on failure
    userTeam = getUserTeam(res)
    unless userTeam?
      return false
    try
      questionData = fs.readFileSync "./questions/#{userTeam}.json"
    catch error
      console.error("Error reading question file: \n", error)
      res.send "Unable to read your team's questions."
      return false
    questionsForUser = JSON.parse(questionData)
    setRobotBrain(res, 'userQuestions', questionsForUser.questions)
    setRobotBrain(res, 'userQuestionsLength', questionsForUser.questions.length)
    return true

  askQuestion = (res) ->
    questionNumber = getRobotBrain(res, 'userQuestionNumber')
    res.send "#{(getRobotBrain(res, 'userQuestions')[questionNumber])}"
    setRobotBrain(res, 'userQuestionNumber', questionNumber + 1)

  addAnswer = (res) ->
    questionNumber = getRobotBrain(res, 'userQuestionNumber')
    questionAndAnswer = ((getRobotBrain(res, 'userQuestions')[questionNumber - 1]) + ": " + (res.match[0].substr 7)) #substr needed to remove the username from hubot response.

    answerArray = getRobotBrain(res, 'userAnswerArray')
    answerArray.push(questionAndAnswer)
    setRobotBrain(res, 'userAnswerArray', answerArray)

    prettyMessage = getRobotBrain(res, 'prettyReportMessage')
    setRobotBrain(res, 'prettyReportMessage', prettyMessage.concat(questionAndAnswer + "\n\n"))

  refreshBotData = (res) ->
    setRobotBrain(res, 'answerCheck', false)

  getUserTeam = (res) ->
    readName = res.envelope.user.name
    try
      userData = fs.readFileSync "./users/#{readName}.json"
    catch error
      console.error("Error reading user file: \n", error)
      res.send "Unable to locate your team. Please configure it using: /changeteam"
      return null
    userData = JSON.parse(userData)
    return userData.team

  setRobotBrain = (res, key, value) ->
    robot.brain.set( (res.envelope.user.name + key), value)

  getRobotBrain = (res, key) ->
    return robot.brain.get(res.envelope.user.name + key)
